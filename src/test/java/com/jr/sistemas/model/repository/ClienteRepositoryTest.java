package com.jr.sistemas.model.repository;

import com.jr.sistemas.model.entities.Cliente;
import com.jr.sistemas.model.entities.Cuenta;
import com.jr.sistemas.model.entities.Movimiento;
import com.jr.sistemas.model.entities.embbeded.MovimientoId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ComponentScan(basePackages = {"com.jr.sistemas.bootstrap"})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ClienteRepositoryTest {

    @Autowired
    ClienteRepository clienteRepository;
    @Autowired
    MovimientoRepository movimientoRepository;
    @Autowired
    CuentaRepository cuentaRepository;

    @Rollback(value = false)
    @Test
    void cuentaRepositoryTest() {
        Cuenta cuenta = new Cuenta("00300002","01","002","0001","00010",5500.50,LocalDate.now(),"CANCELADO",5,"252512");
        Cuenta saved = cuentaRepository.save(cuenta);
        assertThat(saved).isNotNull();

        Cuenta fetched = cuentaRepository.getById(saved.getCodigo());
        assertThat(fetched).isNotNull();
        System.out.println(fetched.getCodigo());
    }

    @Rollback(value = false)
    @Test
    void clienteReposioryTest() {
        Cliente cliente = new Cliente("00021","JAVIER","RODRÍGUEZ","REYES","41966318","LIMA","VILLA EL SALVADOR","TELEFONO","JREYES.SISTEMAS@OUTLOOK.COM");
        Cliente clienteDB = clienteRepository.save(cliente);
        System.out.println(clienteDB.getCodigo());
        assertThat(clienteDB).isNotNull();

        Cliente fetched = clienteRepository.getById(clienteDB.getCodigo());
        assertThat(fetched).isNotNull();
    }

    @Commit
    @Test
    void movimientoCompositeTest() {
        MovimientoId movimientoId = new MovimientoId();
        movimientoId.setCodigo("00300001");
        movimientoId.setNumMovimiento(7L);
        Movimiento movimiento = new Movimiento();
        movimiento.setCodigo(movimientoId.getCodigo());
        movimiento.setNumMovimiento(movimientoId.getNumMovimiento());
        movimiento.setFecha(LocalDate.now());
        movimiento.setEmpCodigo("0001");
        movimiento.setTipoCodigo("004");
        movimiento.setImporte(7500.00);

        Movimiento movimientoSave = movimientoRepository.save(movimiento);
        assertThat(movimientoSave).isNotNull();

        Movimiento feched = movimientoRepository.getById(movimientoId);
        assertThat(feched).isNotNull();

        System.out.println(feched.getCodigo());
    }
}























