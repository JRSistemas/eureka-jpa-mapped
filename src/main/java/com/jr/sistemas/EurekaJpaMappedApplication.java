package com.jr.sistemas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaJpaMappedApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaJpaMappedApplication.class, args);
	}

}
