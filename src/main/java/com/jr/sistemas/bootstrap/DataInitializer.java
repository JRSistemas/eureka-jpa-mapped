package com.jr.sistemas.bootstrap;

import com.jr.sistemas.model.repository.ClienteRepository;
import com.jr.sistemas.model.repository.CuentaRepository;
import com.jr.sistemas.model.repository.MovimientoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer implements CommandLineRunner
{
    private final ClienteRepository clienteRepository;
    private final MovimientoRepository movimientoRepository;
    private final CuentaRepository cuentaRepository;

    public DataInitializer(ClienteRepository clienteRepository,
                           MovimientoRepository movimientoRepository,
                           CuentaRepository cuentaRepository) {
        this.clienteRepository = clienteRepository;
        this.movimientoRepository = movimientoRepository;
        this.cuentaRepository = cuentaRepository;
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
