package com.jr.sistemas.model.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Cuenta {
    @Id
    @Column(name = "chr_cuencodigo")
    private String codigo;
    @Column(name = "chr_monecodigo")
    private String monCodigo;
    @Column(name = "chr_sucucodigo")
    private String sucuCodigo;
    @Column(name = "chr_emplcreacuenta")
    private String empCreaCuenta;
    @Column(name = "chr_cliecodigo")
    private String clieCodigo;
    @Column(name = "dec_cuensaldo")
    private Double  saldo;
    @Column(name = "dtt_cuenfechacreacion")
    private LocalDate fechaCreacion;
    @Column(name = "vch_cuenestado")
    private String estado;
    @Column(name = "int_cuencontmov")
    private Integer movimiento;
    @Column(name = "chr_cuenclave")
    private String clave;

    public Cuenta() {
    }

    public Cuenta(String codigo, String monCodigo, String sucuCodigo, String empCreaCuenta, String clieCodigo, Double saldo, LocalDate fechaCreacion, String estado, Integer movimiento, String clave) {
        this.codigo = codigo;
        this.monCodigo = monCodigo;
        this.sucuCodigo = sucuCodigo;
        this.empCreaCuenta = empCreaCuenta;
        this.clieCodigo = clieCodigo;
        this.saldo = saldo;
        this.fechaCreacion = fechaCreacion;
        this.estado = estado;
        this.movimiento = movimiento;
        this.clave = clave;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMonCodigo() {
        return monCodigo;
    }

    public void setMonCodigo(String monCodigo) {
        this.monCodigo = monCodigo;
    }

    public String getSucuCodigo() {
        return sucuCodigo;
    }

    public void setSucuCodigo(String sucuCodigo) {
        this.sucuCodigo = sucuCodigo;
    }

    public String getEmpCreaCuenta() {
        return empCreaCuenta;
    }

    public void setEmpCreaCuenta(String empCreaCuenta) {
        this.empCreaCuenta = empCreaCuenta;
    }

    public String getClieCodigo() {
        return clieCodigo;
    }

    public void setClieCodigo(String clieCodigo) {
        this.clieCodigo = clieCodigo;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Integer movimiento) {
        this.movimiento = movimiento;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuenta cuenta = (Cuenta) o;
        return Objects.equals(codigo, cuenta.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}
