package com.jr.sistemas.model.entities;

import com.jr.sistemas.model.entities.embbeded.MovimientoId;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import java.time.LocalDate;

@Entity
@IdClass(MovimientoId.class)
public class Movimiento {

    @Id
    @Column(name = "chr_cuencodigo")
    private String codigo;
    @Id
    @Column(name = "int_movinumero")
    private Long numMovimiento;

    @Column(name = "dtt_movifecha")
    private LocalDate fecha;
    @Column(name = "chr_emplcodigo")
    private String empCodigo;
    @Column(name = "chr_tipocodigo")
    private String tipoCodigo;
    @Column(name = "dec_moviimporte")
    private Double importe;
    @Column(name = "chr_cuenreferencia")
    private String referencia;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Long getNumMovimiento() {
        return numMovimiento;
    }

    public void setNumMovimiento(Long numMovimiento) {
        this.numMovimiento = numMovimiento;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getEmpCodigo() {
        return empCodigo;
    }

    public void setEmpCodigo(String empCodigo) {
        this.empCodigo = empCodigo;
    }

    public String getTipoCodigo() {
        return tipoCodigo;
    }

    public void setTipoCodigo(String tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
