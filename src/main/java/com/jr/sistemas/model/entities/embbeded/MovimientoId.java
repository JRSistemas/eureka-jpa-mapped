package com.jr.sistemas.model.entities.embbeded;

import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MovimientoId implements Serializable {
    private String codigo;
    private Long numMovimiento;

    public MovimientoId() {
    }

    public MovimientoId(String codigo, Long numMovimiento) {
        this.codigo = codigo;
        this.numMovimiento = numMovimiento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Long getNumMovimiento() {
        return numMovimiento;
    }

    public void setNumMovimiento(Long numMovimiento) {
        this.numMovimiento = numMovimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovimientoId that = (MovimientoId) o;
        return Objects.equals(codigo, that.codigo) && Objects.equals(numMovimiento, that.numMovimiento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo, numMovimiento);
    }
}
