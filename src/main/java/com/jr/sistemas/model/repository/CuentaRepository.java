package com.jr.sistemas.model.repository;

import com.jr.sistemas.model.entities.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaRepository extends JpaRepository<Cuenta, String> {
}
