package com.jr.sistemas.model.repository;

import com.jr.sistemas.model.entities.Movimiento;
import com.jr.sistemas.model.entities.embbeded.MovimientoId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovimientoRepository extends JpaRepository<Movimiento, MovimientoId> {
}
